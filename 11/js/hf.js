//írd ki konzolra 1 -100 ig a páros számok összegét
//2550
let sum = 0;
for(let i = 2; i <= 100; i = i +2){
    sum += i;//növeljük i vel az összeget
}
console.log('A páros számok összege 1 - 100 ig: ' + sum);
//szorozd össze a számokat 7ig és írd ki az eredményt a konzolra (7!)
//5040
let mtpl = 1;//itt lesz a szorzat
for(let i = 1; i <= 7; i++){
    mtpl *= i;//növeljük i vel az összeget
}
console.log('A számok szorzata 1 - 7 ig: ' + mtpl);
//készíts egy id-val ellátott taget, és változtasd meg a háttérszínét és a tartalmát javascript segítségével
let ujElem = '<div id="ujElem">új elem</div>';//létrehozzuk egy változóba
document.body.insertAdjacentHTML( 'beforeend', ujElem );//hozzáadjuk a body-hoz

//átszinezés
ujElem = document.getElementById('ujElem');
ujElem.style.backgroundColor = 'lime';