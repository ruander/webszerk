/*
console.log(document);
console.log(window);
console.log(document.getElementById('app'));//pl egy elem kiválasztása az oldalról id alapján
*/

//eljárások, újrafelhasználható kódrészletek
function myLog() {
    let szoveg = "Ez egy teszt szöveg!";
    console.log(szoveg);
}
myLog();
myLog();

//eljárások paraméterezhetősége
function welcome(name) {
    let greeting = "Welcome " + name + "!";
    console.log(greeting);
}
welcome('George');

//html tag tartalmának kiolvasása
let elem = document.getElementById('app');
console.log(elem.innerHTML);

//html elem megváltoztatása
elem.innerHTML = 'woohoo';//html tartalom felülírása
elem.append(' teszt');//elem bővítése
elem.style.backgroundColor = 'pink';

//a gomb
let myBtn = document.getElementById('testBtn');
console.log(myBtn);


