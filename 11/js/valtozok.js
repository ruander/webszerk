/*
    Változók
    típusai:
     - primitvek:
        egész számok (integer)
        lebegő pontos számok (floating point, float)
        szöveg (string)
        logikai (bolean, bool) - true, false
     - tömbök (array)
     - objektumok (object)
     */
var number = 5;//operátorok : = -> értékadó operátor
var lebego = 7/6; // /, osztás, * szorzás, + összeadás, - kivonás
let szoveg = "My name is Earl";// "" vagy '' - string operátor
let logikai = true;

console.log(number);
console.log(lebego,szoveg,logikai);
//Műveletek a változókkal
let a = 6;
let b = 15;
//
a = a + 5;
b = b - a;
console.log(b);//4
a += 5;
b -= a;// anológ módon működik a *= /=
console.log(b);//-12

//változó tipusa
console.log(typeof number);
console.log(typeof lebego);
console.log(typeof szoveg);
console.log(typeof logikai);
console.log(typeof a);
console.log(typeof b);
console.log(typeof console);
//művelet nem azonos tipusok között
let teszt = a + " Hello world!";
console.log(teszt, typeof teszt);//stringgé konvertálódik

let maradek = 11 % 3;// maradékos osztás:  %
console.log(maradek);//2

// az escape operátor
let idezet = "Loerm ipsum szerint:\n\t \"A föld balra forog, vagy nem\" ! ";// \n uj sor, \t tab
console.log(idezet);
idezet = 'Valaki más szerint: \n "Nem így volt" !';
console.log(idezet);

//tömbök
let tomb = [10,25,43];
console.log(tomb,tomb[0]);//tömb 1 eleme tomb[index]

//tömb 1 elemének módosítása
tomb[1] = 135/32;
console.log(tomb);

//szöveg vagy tömb hossza
let szovegHossz = idezet.length;
let tombHossz = tomb.length;
console.log(szovegHossz,tombHossz);
console.log(tomb[2],idezet[0]);//43 V

//objektum
let myObject = {
    valami: 'Teszt',
    id: 5,
    email: 'hgy@ruander.hu'
};
console.log(myObject);
//objektum 1 értékének kiírása console-ra
console.log(myObject.email);
console.log(myObject['email']);//működik!
//console.log(tomb.0)//nem működik!!!!!!