//alapvető programozástechnikai elemek 2, elágazás és ciklus

let a = 4;

//ha páros az 'a' akkor legyen piros a body háttere, egyébként világoskék
/*
if(feltétel){
    igaz
}else{
    hamis
}
 */
var myBodyStyle = document.body;
if(a % 2 == 0){// == egyenlőség vizsgálat
    myBody.style.backgroundColor = 'red';
}else{
    myBody.style.backgroundColor = 'lightblue';
}
//ciklus
/*
for(ciklusváltozó kezeti értékenek beállítása;feltétel;ciklusváltozó léptetése){
    ciklusmag
}
 */
for(let i = 0; i < 5 ; i++ ){
    console.log(i);
}

//tömb bejárása
let tomb = [
    'Hello',
    'World',
    3.14,
    32,
    true
];
let tombHossz = tomb.length;
for(i = 0; i < tombHossz; i++) {
    //tomb aktuális i-edik eleme:
    console.log(tomb[i]);
}
//saját eljárás véletlenszámhoz 1
function myRandomGenerator() {
    //véletlen szám generálás 0 és 100 közé
    let randomNumber = Math.random() * 100;//0.xxxx-100
    randomNumber = Math.round(randomNumber);

    return randomNumber;
}
//nézzük meg Schrödinger macskáját
let randomNumber = myRandomGenerator();
if(randomNumber %2 == 0){
    var myText = 'A macska él!';
}else{
    var myText = 'A macska megnyekkent!';
}

document.getElementById('app').innerHTML = myText;
//dobokockadobás szimulátor
let testNumber = Math.floor(Math.random()*6+1) ;

console.log(testNumber);
//HF: próbáljunk 7 és 11 közé generálni értéket
testNumber = Math.random();

console.log(testNumber);//7-11
//paraméterezhető véletlenszám generátor
function myRandomGenerator2(tol,ig){

}